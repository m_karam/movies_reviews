class CreateMovies < ActiveRecord::Migration[7.1]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :director
      t.text :description
      t.string :year
      t.decimal :average_stars

      t.timestamps
    end

    add_index(:movies, :name, if_not_exists: true, unique: true)
  end
end
