json.extract! location, :id, :city, :country, :created_at, :updated_at
json.url location_url(location, format: :json)
