class Movie < ApplicationRecord
  has_many :movie_actors
  has_many :movie_locations
  has_many :actors, through: :movie_actors
  has_many :locations, through: :movie_locations
  has_many :reviews

  accepts_nested_attributes_for :actors
  accepts_nested_attributes_for :locations
  accepts_nested_attributes_for :reviews

  validates :name, uniqueness: { case_sensitive: false }

  def update_average_stars
    self.average_stars = reviews.average(:stars).to_f
  end
end
