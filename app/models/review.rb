class Review < ApplicationRecord
  belongs_to :movie
  after_commit :update_average_stars

  def update_average_stars
    movie.update_average_stars
    movie.save
  end
end
