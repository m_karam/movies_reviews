class Actor < ApplicationRecord
  has_many :movies_actors
  has_many :movies, through: :movie_actors

  scope :search, ->(query) { where('name LIKE ?', sanitize_sql_like(query) + '%') }
end
