namespace :import do
  desc "Import movies"
  task movies: :environment do |_, args|
    require 'smarter_csv'

    movies_file_path = args[:movies_file] || '/home/mohamedkaram/Desktop/movies.csv'
    reviews_file_path = args[:reviews_file] || '/home/mohamedkaram/Desktop/reviews.csv'

    csv_options = { chunk_size: 100 }

    SmarterCSV.process(movies_file_path, csv_options) do |batch|
      movies = {}
      batch.each do |row|
        movies[row[:movie]] ||= {
          name: row[:movie], description: row[:description], director: row[:director],
          year: row[:year], actors_attributes: Set.new, locations_attributes: Set.new
        }
        movies[row[:movie]][:actors_attributes].add({name: row[:actor]})
        movies[row[:movie]][:locations_attributes].add({city: row[:filming_location], country: row[:country]})
      end

      data = movies.values.map do |movie|
        movie[:actors_attributes] = movie[:actors_attributes].to_a
        movie[:locations_attributes] = movie[:locations_attributes].to_a
        movie
      end
      Movie.create data.to_a
    end

    SmarterCSV.process(reviews_file_path, csv_options) do |batch|
      movies = {}
      reviews = []
      batch.each do |row|
        movies[row[:movie]] ||= Movie.find_by(name: row[:movie])
        review = {user: row[:user], content: row[:review], stars: row[:stars], movie_id: movies[row[:movie]].id}
        reviews << review
      end
      Review.create reviews
      update_average_stars movies.values
    end

  end

  def update_average_stars(movies)
    movies.each do |movie|
      movie.update_average_stars
      movie.save
    end
  end
end
